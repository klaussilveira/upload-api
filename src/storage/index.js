Object.defineProperty(exports, "Redis", {
  configurable: true,
  enumerable: true,

  get() {
    return require("./redis");
  }
});
