const redis = require("redis");
const namespace = "files:";

module.exports = class Redis {
  constructor() {
    this.client = redis.createClient({ url: process.env.REDIS_DSN });
  }

  save(file) {
    return new Promise((resolve, reject) => {
      delete file.buffer;
      this.client.set(
        namespace + file.uuid,
        JSON.stringify(file),
        (error, response) => {
          if (error) {
            return reject(error);
          }

          resolve(file);
        }
      );
    });
  }

  find(uuid) {
    return new Promise((resolve, reject) => {
      this.client.get(namespace + uuid, (error, response) => {
        if (error || !response) {
          return reject();
        }

        resolve(JSON.parse(response.toString()));
      });
    });
  }

  list() {
    return new Promise((resolve, reject) => {
      this.client.keys(namespace + "*", (error, keys) => {
        if (error) {
          return reject(error);
        }

        this.client.mget(keys, (error, values) => {
          if (error) {
            return reject(error);
          }

          var files = [];

          values.forEach(value => {
            files.push(JSON.parse(value));
          });

          resolve(files);
        });
      });
    });
  }
};
