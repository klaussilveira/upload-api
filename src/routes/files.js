const express = require("express");
const router = express.Router();
const filesystem = require("../filesystem");
const uuidv4 = require("uuid/v4");

router.post("/upload", (req, res, next) => {
  if (!req.files) {
    res.status(400).json({ code: 400, message: "No files were provided." });

    return;
  }

  const filesystemAdapter = new filesystem[process.env.FILESYSTEM_ADAPTER]();
  const uploadedFiles = [];

  for (index in req.files) {
    var file = {
      uuid: uuidv4(),
      name: req.files[index].name,
      mimetype: req.files[index].mimetype,
      buffer: req.files[index].data,
      size: req.files[index].data.length
    };

    uploadedFiles.push(filesystemAdapter.save(file));
  }

  Promise.all(uploadedFiles)
    .then(files => {
      res.json(files);
    })
    .catch(() => {
      res.status(400).json({ code: 400, message: "Invalid file." });
    });
});

router.get("/:uuid", (req, res) => {
  const filesystemAdapter = new filesystem[process.env.FILESYSTEM_ADAPTER]();

  filesystemAdapter
    .find(req.params.uuid)
    .then(file => {
      res.setHeader("Content-Disposition", "attachment; filename=" + file.name);
      res.setHeader("Content-Type", file.mimetype);
      res.setHeader("Content-Length", file.size);
      file.buffer.pipe(res);
    })
    .catch(() => {
      res.status(404).json({ code: 404, message: "Unable to find file." });
    });
});

router.get("/", (req, res) => {
  const filesystemAdapter = new filesystem[process.env.FILESYSTEM_ADAPTER]();

  filesystemAdapter
    .list()
    .then(files => {
      res.json(files);
    })
    .catch(() => {
      res.status(404).json({ code: 404, message: "Unable to find file." });
    });
});

module.exports = router;
