const express = require("express");
const fileUpload = require("express-fileupload");
const filesRouter = require("./routes/files");
const errorHandler = require("./errorHandler");

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(fileUpload());
app.use("/files", filesRouter);
app.use(errorHandler);

module.exports = app;
