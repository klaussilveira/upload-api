const AWS = require("aws-sdk");
const storage = require("../storage");

module.exports = class S3 {
  constructor() {
    this.client = new AWS.S3({ region: process.env.S3_REGION });
    this.storage = new storage[process.env.STORAGE_ADAPTER]();
  }

  save(file) {
    return new Promise((resolve, reject) => {
      this.client
        .upload({
          Bucket: process.env.S3_BUCKET,
          Key: file.uuid,
          Body: file.buffer
        })
        .promise()
        .then(() => {
          this.storage
            .save(file)
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    });
  }

  find(uuid) {
    return new Promise((resolve, reject) => {
      this.storage.find(uuid).then(file => {
        const objectRequest = {
          Bucket: process.env.S3_BUCKET,
          Key: uuid
        };

        this.client
          .getObject(objectRequest)
          .promise()
          .then(() => {
            file.buffer = this.client
              .getObject(objectRequest)
              .createReadStream();
            resolve(file);
          })
          .catch(reject);
      });
    });
  }

  list() {
    return this.storage.list();
  }
};
