Object.defineProperty(exports, "S3", {
  configurable: true,
  enumerable: true,

  get() {
    return require("./s3");
  }
});
