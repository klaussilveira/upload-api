# upload-api

Simple file upload service.

## Dependencies

- node.js 8
- redis
- S3

## Setup

    $ yarn install

Local environments can be easily configured via `.env` files. Copy the
template one and configure with your details:

    $ cp .env.dist .env

## Run

    $ yarn start

## Docker

You can also use Docker to setup.

    $ docker-compose up

## Documentation

OpenAPI 3 documentation can be found at `docs/api.yaml`.

## Staging

Staging environment can be found at `https://infinite-springs-63955.herokuapp.com`.
